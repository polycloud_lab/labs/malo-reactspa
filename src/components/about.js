export default function About() {
  return (
    <section className="hero is-primary">
      <div className="hero-body">
        <h1 className="title">About me</h1>
        <p className="subtitle">
          My name is <strong>Bob</strong>, a young developer aiming to be a
          great DevOps engineer !
        </p>
      </div>
    </section>
  );
}
